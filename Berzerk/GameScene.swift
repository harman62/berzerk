//
//  GameScene.swift
//  Berzerk
//
//  Created by Harman Kaur on 2019-10-10.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
  
    //MARK: outlets for sprites
    var player:SKSpriteNode!
    let PLAYER_SPEED : CGFloat = 20
    
    override func didMove(to view: SKView) {
        //SKPhysicscontactdelegate setup
        self.physicsWorld.contactDelegate = self
        //initialize the player
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        //setup physics for player
//        self.player.physicsBody = SKPhysicsBody(edgeFrom: <#T##CGPoint#>, to: <#T##CGPoint#>)
//        self.player.physicsBody?.contactTestBitMask = 10
//        self.player.physicsBody?.collisionBitMask = 4
//        self.player.physicsBody?.categoryBitMask = 1
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //get the mouse position where it was clicked
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        
        let location = mouseTouch!.location(in: self)
        //what node did player touch
        let nodeTouched = atPoint(location).name
        print("Player Touched: \(nodeTouched)")
        
        //game logic: move player based on touch
        if (nodeTouched == "up") {
            //move up
            self.player.position.y = self.player.position.y + PLAYER_SPEED
        }
        else if (nodeTouched == "down") {
            //move down
            self.player.position.y = self.player.position.y - PLAYER_SPEED
        }
        else if (nodeTouched == "left") {
            //move left
            self.player.position.x = self.player.position.x - PLAYER_SPEED
        }
        else if (nodeTouched == "right") {
            //move right
            self.player.position.x = self.player.position.x + PLAYER_SPEED
        }
        
    }
    
    //detect when collision occurs
    func didBegin(_ contact: SKPhysicsContact) {
        
    }
}
